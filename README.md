# Pokemon REST API

[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

Pokemon Rest API is a [Spring Boot](http://projects.spring.io/spring-boot/) application.

## Requirements

For building and running the application you need:

- [Java 11](https://www.oracle.com/java/technologies/downloads/#java11)
- [Maven 3](https://maven.apache.org/download.cgi)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.petalmd.pokem.PokemonApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html), to do this you need to:

1. Clone the repository
```shell
   git clone https://bitbucket.org/dquiroz123/pokemon.git
```
2. open a terminal in the `pokemon` folder (the root project folder)
3. execute the command



```shell
      mvn spring-boot:run
```
Note: the app runs on the 8080 port, if you need to modify the port you can do it updating the `application.properties` file.

## Using the REST endpoints
### List Operation
The List operation is available on: GET http://localhost:8080/rs/pokemon/list. The results  returned by this operation are paginated, it allows the following query params:

* size: number of elements present in a page, the default value is 20.
* page: page number to consult, the default value is 0.
* sort: to have the results sorted on a particular property, add this parameter with the name of the property you want to sort the results on. You can control the direction of the sort by adding the values desc(descending) or asc (ascending).

Examples:

* GET http://localhost:8080/rs/pokemon/list?size=5&page=0&sort=name,asc : get the first 5 pokemons ordered by name (ascending way)
* GET http://localhost:8080/rs/pokemon/list : get the first 20 pokemons, the order is determined by the database
* GET http://localhost:8080/rs/pokemon/list?size=800 : get the first 800 pokemons, the order is determined by the database

The list operation is based on the hateoas principle, additional metadata is added to every request, this metadata is explained as follows:

* _links: contains information related with the navigation through the pagination results, it is made up of five elements, a link to first page, a link to the previous page, a link to current page, a link to next page, and a link to the last page.
```json
{ "_links":{"first":{"href":"http://localhost:8080/rs/pokemon/list?page=0&size=5&sort=name,asc"},"self":{"href":"http://localhost:8080/rs/pokemon/list?page=0&size=5&sort=name,asc"},"next":{"href":"http://localhost:8080/rs/pokemon/list?page=1&size=5&sort=name,asc"},"last":{"href":"http://localhost:8080/rs/pokemon/list?page=159&size=5&sort=name,asc"}} }
```
* page: shows a summary of the data, for instance: total elements in the database, total pages, current page and page size
```json
{"page":{"size":5,"totalElements":800,"totalPages":160,"number":0}}
  ```

### Create Operation
The Create operation is available on: POST http://localhost:8080/rs/pokemon, it requires the pokemon information as part of the request body, this information should be a JSON, like the example below: 

```json
{"#":1,"Name":"new pokemon","Type 1":"Grass","Type 2":"Poison","Total":318,"HP":45,"Attack":49,"Defense":49,"Sp. Atk":65,"Sp. Def":65,"Speed":45,"Generation":1,"Legendary":false}
```

Using Postman, a setup to use the Create operation would look like:
![Alt text](documents/images/postman-create.png?raw=true "Title")

some considerations:

* The pokemon name is unique, if you try to create a pokemon using an already name stored in the database then the operation will not be successful.
* The only attribute that is optional is `Type 2`, all other attributes have to be included as part of the request body, if any of the required attributes is missing then the app will return a HTTP bad request response.
* the `Type 1` and `Type 2` values have to be one of the values defined in the `TypeEnum` Java file, otherwise the app will return a bad request response.

### Update Operation
The Update operation is available on: PUT http://localhost:8080/rs/pokemon, it requires the pokemon information as part of the request body, the JSON format is the same used by the Create operation:

some considerations:

* You can update a pokemon only if it was previously created, if you try to update a pokemon that does not exist in the database then the operation will not be successful. The app uses the pokemon name to determinate if a pokemon was created or not.
* The only attribute that is optional is `Type 2`, all other attributes have to be included as part of the request body, if any of the required attributes is missing then the app will return a HTTP bad request response.
* the `Type 1` and `Type 2` values have to be one of the values defined in the `TypeEnum` Java file, otherwise the app will return a bad request response.


### Read Operation
The read operation is available in two endpoints, one for searching by pokemon name, and the other by pokemon number: 

* GET http://localhost:8080/rs/pokemon/{pokemon-name}, example: http://localhost:8080/rs/pokemon/bulbasaur, when you use this endpoint, the pokemon name is not case-sensitive, so you can use ../rs/pokemon/bulbasaur or ../rs/pokemon/Bulbasaur.
* GET http://localhost:8080/rs/pokemon/number/{pokemon-number}, example: http://localhost:8080/rs/pokemon/number/6

In both cases, if the pokemon does not exist in the database then the app will return an HTTP not found error.

### Delete Operation

The read operation is available in two endpoints, one for searching by pokemon name, and the other by pokemon number:

* DELETE http://localhost:8080/rs/pokemon/{pokemon-name}, when you use this endpoint, the pokemon name is not case-sensitive.
* DELETE http://localhost:8080/rs/pokemon/number/{pokemon-number}.

In both cases, the app will return a message indicating how many pokemons were deleted.


### Future Work on this app

* Increase the unit test code coverage, currently it has a few unit tests.
* Add javadoc to the java files.
* Add more log entries.
* Document the REST API using Swagger.
* Integrate the project with docker to give the users another alternative to run it.
* Add a PATCH operation to allow users to update only the pokemon attributes that they need to update (without sending all the pokemon attributes).

## Copyright

Released under the Apache License 2.0. See the [LICENSE](https://www.apache.org/licenses/LICENSE-2.0) file.