package com.petalmd.pokemon.service;

import com.petalmd.pokemon.assembler.PokemonAssembler;
import com.petalmd.pokemon.entity.PokemonEntity;
import com.petalmd.pokemon.exception.ExistException;
import com.petalmd.pokemon.exception.NonExistException;
import com.petalmd.pokemon.model.PokemonModel;
import com.petalmd.pokemon.repository.PokemonRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedModel;
import org.springframework.data.web.PagedResourcesAssembler;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PokemonService {
    private static final String DELETED_MESSAGE = "Total pokemons deleted: {0}";

    private PokemonRepository pokemonRepository;
    private PokemonAssembler pokemonAssembler;
    private PagedResourcesAssembler<PokemonEntity> pagedResourcesAssembler;
    private ModelMapper modelMapper;

    public PokemonService(PokemonRepository pokemonRepository, PokemonAssembler pokemonAssembler,
                          PagedResourcesAssembler pagedResourcesAssembler, ModelMapper modelMapper) {
        this.pokemonRepository = pokemonRepository;
        this.pokemonAssembler = pokemonAssembler;
        this.pagedResourcesAssembler = pagedResourcesAssembler;
        this.modelMapper = modelMapper;
    }

    public PokemonModel getPokemonByName(String pokemonName) {
        PokemonEntity pokemonEntity = pokemonRepository.getPokemonByNameIgnoreCase(pokemonName);
        PokemonModel pokemonModel = null;
        if (pokemonEntity != null) {
            pokemonModel = pokemonAssembler.toModel(pokemonEntity);
        }
        return pokemonModel;
    }

    public List<PokemonModel> getPokemonsByNumber(int pokemonNumber) {
        List<PokemonEntity> list = pokemonRepository.getPokemonsByNumber(pokemonNumber);
        List<PokemonModel> models = list.stream()
                .map(pokemonEntity -> pokemonAssembler.toModel(pokemonEntity))
                .collect(Collectors.toList());
        return models;
    }

    public PokemonModel savePokemon(PokemonModel pokemonModel) throws ExistException{
        PokemonEntity pokemon = pokemonRepository.getPokemonByNameIgnoreCase(pokemonModel.getName());
        if(pokemon != null){
            throw new ExistException(pokemonModel.getName());
        }
        PokemonEntity pokemonEntity = pokemonRepository.save(modelMapper.map(pokemonModel, PokemonEntity.class));
        PokemonModel result = null;
        if (pokemonEntity != null) {
            result = pokemonAssembler.toModel(pokemonEntity);
        }
        return result;
    }

    public PokemonModel updatePokemon(PokemonModel pokemonModel) throws NonExistException {
        PokemonEntity pokemon = pokemonRepository.getPokemonByNameIgnoreCase(pokemonModel.getName());
        if(pokemon == null){
            throw new NonExistException(pokemonModel.getName());
        }
        pokemonModel.setName(pokemon.getName());
        PokemonEntity pokemonEntity = pokemonRepository.save(modelMapper.map(pokemonModel, PokemonEntity.class));
        PokemonModel result = null;
        if (pokemonEntity != null) {
            result = pokemonAssembler.toModel(pokemonEntity);
        }
        return result;
    }

    public PagedModel<PokemonModel> findAll(Pageable pageable) {
        Page<PokemonEntity> pokemonEntities = pokemonRepository.findAll(pageable);
        PagedModel<PokemonModel> pagedModel = pagedResourcesAssembler.toModel(pokemonEntities, pokemonAssembler);
        return pagedModel;
    }

    public String deleteByName(String name) {
        Long deleted = pokemonRepository.deleteByNameIgnoreCase(name);
        return getDeletedMessage(deleted);
    }

    public String deleteByNumber(int number) {
        Long deleted = pokemonRepository.deleteByNumber(number);
        return getDeletedMessage(deleted);
    }

    private String getDeletedMessage(Long deleted) {
        return MessageFormat.format(DELETED_MESSAGE, deleted);
    }

}
