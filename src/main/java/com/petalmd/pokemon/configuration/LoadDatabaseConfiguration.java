package com.petalmd.pokemon.configuration;

import com.petalmd.pokemon.entity.PokemonEntity;
import com.petalmd.pokemon.model.PokemonModel;
import com.petalmd.pokemon.repository.PokemonRepository;
import com.petalmd.pokemon.util.ObjectTransformer;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Slf4j
@Configuration
class LoadDatabaseConfiguration {

    @Bean
    CommandLineRunner initDatabase(PokemonRepository repository, ObjectTransformer objectTransformer, ModelMapper modelMapper) {

        return args -> {
            List<PokemonModel> pokemonEntities = objectTransformer.loadObjectList(PokemonModel.class, "/static/pokemon.csv");
            pokemonEntities.forEach(pokemon ->
                log.info("Preloading " + repository.save(modelMapper.map(pokemon, PokemonEntity.class)))
            );
        };
    }
}
