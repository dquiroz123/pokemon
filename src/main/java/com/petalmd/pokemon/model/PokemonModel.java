package com.petalmd.pokemon.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.petalmd.pokemon.util.TypeEnum;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.core.Relation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Relation(collectionRelation = "pokemons", itemRelation = "pokemon")
@JsonInclude(Include.NON_NULL)
public class PokemonModel extends RepresentationModel<PokemonModel>{
    @JsonProperty("#")
    @NotNull(message = "the attribute <#> is mandatory")
    private int number;

    @JsonProperty("Name")
    @NotBlank(message = "the attribute <Name> is mandatory")
    private String name;

    @JsonProperty("Type 1")
    @NotNull(message = "the attribute <Type 1> is mandatory")
    @Enumerated(EnumType.STRING)
    private TypeEnum type1;

    @JsonProperty("Type 2")
    @Enumerated(EnumType.STRING)
    private TypeEnum type2;

    @JsonProperty("Total")
    @NotNull(message = "the attribute <Total> is mandatory")
    private Integer total;

    @JsonProperty("HP")
    @NotNull(message = "the attribute <HP> is mandatory")
    private Integer hp;

    @JsonProperty("Attack")
    @NotNull(message = "the attribute <Attack> is mandatory")
    private Integer attack;

    @JsonProperty("Defense")
    @NotNull(message = "the attribute <Defense> is mandatory")
    private Integer defense;

    @JsonProperty("Sp. Atk")
    @NotNull(message = "the attribute <Sp. Atk> is mandatory")
    private Integer spAtk;

    @JsonProperty("Sp. Def")
    @NotNull(message = "the attribute <Sp. Def> is mandatory")
    private Integer spDef;

    @JsonProperty("Speed")
    @NotNull(message = "the attribute <Speed> is mandatory")
    private Integer speed;

    @JsonProperty("Generation")
    @NotNull(message = "the attribute <Generation> is mandatory")
    private Integer generation;

    @JsonProperty("Legendary")
    @NotNull(message = "the attribute <Legendary> is mandatory")
    private boolean legendary;
}
