package com.petalmd.pokemon.rest;

import com.petalmd.pokemon.exception.ExistException;
import com.petalmd.pokemon.exception.NonExistException;
import com.petalmd.pokemon.model.PokemonModel;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RequestMapping("rs/pokemon")
public interface PokemonController {
    @GetMapping("/{pokemonName}")
    ResponseEntity<PokemonModel> getPokemonByName(@PathVariable("pokemonName") String pokemonName);

    @GetMapping("/number/{pokemonNumber}")
    ResponseEntity<List<PokemonModel>> getPokemonsByNumber(@PathVariable("pokemonNumber") int pokemonNumber);

    @PostMapping()
    ResponseEntity<PokemonModel> savePokemon( @RequestBody @Valid PokemonModel pokemonModel) throws ExistException;

    @PutMapping()
    ResponseEntity updatePokemon( @RequestBody @Valid PokemonModel pokemonModel) throws NonExistException;

    @GetMapping("/list")
    ResponseEntity<PagedModel<PokemonModel>> getAllPokemons(Pageable pageable);

    @DeleteMapping(value = "/{pokemonName}")
    ResponseEntity deleteByName(@PathVariable("pokemonName") String pokemonName);

    @DeleteMapping(value = "/number/{pokemonNumber}")
    ResponseEntity deleteByNumber(@PathVariable("pokemonNumber") int pokemonNumber);
}
