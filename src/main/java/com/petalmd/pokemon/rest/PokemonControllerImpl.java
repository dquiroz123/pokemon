package com.petalmd.pokemon.rest;

import com.petalmd.pokemon.assembler.PokemonAssembler;
import com.petalmd.pokemon.exception.CustomHandlerException;
import com.petalmd.pokemon.exception.ExistException;
import com.petalmd.pokemon.exception.NonExistException;
import com.petalmd.pokemon.model.PokemonModel;
import com.petalmd.pokemon.service.PokemonService;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class PokemonControllerImpl extends CustomHandlerException implements PokemonController{
    private PokemonService pokemonService;

    public PokemonControllerImpl(PokemonService pokemonService, PokemonAssembler pokemonAssembler) {
        this.pokemonService = pokemonService;
    }

    @Override
    public ResponseEntity<PokemonModel> getPokemonByName(String pokemonName) {
        PokemonModel pokemonModel = pokemonService.getPokemonByName(pokemonName);
        if(pokemonModel != null) {
            return ResponseEntity.status(HttpStatus.OK).body(pokemonModel);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @Override
    public ResponseEntity<List<PokemonModel>> getPokemonsByNumber(int pokemonNumber) {
        List<PokemonModel> pokemons = pokemonService.getPokemonsByNumber(pokemonNumber);
        if(pokemons != null && pokemons.size() > 0) {
            return ResponseEntity.status(HttpStatus.OK).body(pokemons);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @Override
    public ResponseEntity<PokemonModel> updatePokemon(PokemonModel pokemonModel) throws NonExistException {
        PokemonModel result = pokemonService.updatePokemon(pokemonModel);
        if(result != null) {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @Override
    public ResponseEntity<PokemonModel> savePokemon(PokemonModel pokemonModel) throws ExistException {
        PokemonModel result = pokemonService.savePokemon(pokemonModel);
        if(result != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(result);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @Override
    public ResponseEntity<PagedModel<PokemonModel>> getAllPokemons(Pageable pageable) {
        PagedModel<PokemonModel> pagedModel = pokemonService.findAll(pageable);
        return new ResponseEntity<>(pagedModel,HttpStatus.OK);
    }

    @Override
    public ResponseEntity deleteByName(String pokemonName) {
        String result = pokemonService.deleteByName(pokemonName);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @Override
    public ResponseEntity deleteByNumber(int pokemonNumber) {
        String result = pokemonService.deleteByNumber(pokemonNumber);
        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

}
