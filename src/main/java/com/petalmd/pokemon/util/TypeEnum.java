package com.petalmd.pokemon.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.petalmd.pokemon.exception.WrongTypeException;

import java.util.Arrays;
import java.util.Optional;

public enum TypeEnum {
    @JsonProperty("Bug")
    BUG("Bug"),
    @JsonProperty("Dark")
    DARK("Dark"),
    @JsonProperty("Dragon")
    DRAGON("Dragon"),
    @JsonProperty("Electric")
    ELECTRIC("Electric"),
    @JsonProperty("Empty")
    Empty(""),
    @JsonProperty("Fairy")
    FAIRY("Fairy"),
    @JsonProperty("Fighting")
    FIGHTING("Fighting"),
    @JsonProperty("Fire")
    FIRE("Fire"),
    @JsonProperty("Flying")
    FLYING("Flying"),
    @JsonProperty("Ghost")
    GHOST("Ghost"),
    @JsonProperty("Grass")
    GRASS("Grass"),
    @JsonProperty("Ground")
    GROUND("Ground"),
    @JsonProperty("Ice")
    ICE("Ice"),
    @JsonProperty("Normal")
    NORMAL("Normal"),
    @JsonProperty("Poison")
    POISON("Poison"),
    @JsonProperty("Psychic")
    PSYCHIC("Psychic"),
    @JsonProperty("Rock")
    ROCK("Rock"),
    @JsonProperty("Steel")
    STEEL("Steel"),
    @JsonProperty("Water")
    WATER("Water");

    public final String type;

    TypeEnum(String type) {
        this.type = type;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static TypeEnum forType(String type) throws Exception{
        Optional<TypeEnum> optionalEnum = Arrays.stream(values())
                .filter(typeEnum -> typeEnum.type.equalsIgnoreCase(type))
                .findAny();
         if(optionalEnum.isPresent()){
             return optionalEnum.get();
         } else {
             throw new WrongTypeException(type);
         }
    }
}
