package com.petalmd.pokemon.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.petalmd.pokemon.util.TypeEnum;
import lombok.Data;

import javax.persistence.*;


@Entity
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PokemonEntity {

    @Column(nullable = false)
    private int number;

    @Id
    private String name;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TypeEnum type1;

    @Enumerated(EnumType.STRING)
    private TypeEnum type2;

    @Column(nullable = false)
    private Integer total;

    @Column(nullable = false)
    private Integer hp;

    @Column(nullable = false)
    private Integer attack;

    @Column(nullable = false)
    private Integer defense;

    @Column(nullable = false)
    private Integer spAtk;

    @Column(nullable = false)
    private Integer spDef;

    @Column(nullable = false)
    private Integer speed;

    @Column(nullable = false)
    private Integer generation;

    @Column(nullable = false)
    private boolean legendary;
}
