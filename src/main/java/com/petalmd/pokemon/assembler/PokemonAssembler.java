package com.petalmd.pokemon.assembler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.petalmd.pokemon.entity.PokemonEntity;
import com.petalmd.pokemon.model.PokemonModel;
import com.petalmd.pokemon.rest.PokemonController;
import org.modelmapper.ModelMapper;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class PokemonAssembler extends RepresentationModelAssemblerSupport<PokemonEntity, PokemonModel> {

    private ModelMapper modelMapper;

    public PokemonAssembler(ModelMapper modelMapper) {
        super(PokemonController.class, PokemonModel.class);
        this.modelMapper = modelMapper;
    }

    @Override
    public PokemonModel toModel(PokemonEntity entity) {
        PokemonModel pokemonModel = modelMapper.map(entity, PokemonModel.class);
        Link link = linkTo(methodOn(PokemonController.class)
                        .getPokemonByName(entity.getName()))
                        .withSelfRel();
        pokemonModel.add(link);
        return pokemonModel;
    }
}
