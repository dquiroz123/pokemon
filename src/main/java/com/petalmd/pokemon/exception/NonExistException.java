package com.petalmd.pokemon.exception;

import java.text.MessageFormat;

public class NonExistException extends Exception{
    public NonExistException(String name){
        super(MessageFormat.format("Pokemon with name <{0}> does not exist", name));
    }
}
