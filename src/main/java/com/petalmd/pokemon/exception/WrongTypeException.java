package com.petalmd.pokemon.exception;

import java.text.MessageFormat;

public class WrongTypeException extends Exception{
    public WrongTypeException(String wrongType){
        super(MessageFormat.format("Pokemon type <{0}> not allowed", wrongType));
    }
}
