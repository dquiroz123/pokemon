package com.petalmd.pokemon.exception;

import java.text.MessageFormat;

public class ExistException extends Exception{
    public ExistException(String name){
        super(MessageFormat.format("Pokemon with name <{0}> already exist", name));
    }
}
