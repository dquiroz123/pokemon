package com.petalmd.pokemon.repository;

import com.petalmd.pokemon.entity.PokemonEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PokemonRepository extends PagingAndSortingRepository<PokemonEntity, String> {
    PokemonEntity getPokemonByNameIgnoreCase(@Param("name") String pokemonName);
    List<PokemonEntity> getPokemonsByNumber(@Param("number") int number);
    Long deleteByNumber(@Param("number") int number);
    Long deleteByNameIgnoreCase(@Param("name") String name);
}
