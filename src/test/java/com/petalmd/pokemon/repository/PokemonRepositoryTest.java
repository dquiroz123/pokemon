package com.petalmd.pokemon.repository;

import com.petalmd.pokemon.entity.PokemonEntity;
import com.petalmd.pokemon.util.TypeEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


@ExtendWith(SpringExtension.class)
@DataJpaTest
public class PokemonRepositoryTest {

    @Autowired
    private PokemonRepository pokemonRepository;

    @Test
    public void should_create_new_pokemon() {
        savePokemon();
        PokemonEntity pokemonEntity = pokemonRepository.getPokemonByNameIgnoreCase("Bulbasaur");
        Assertions.assertNotNull(pokemonEntity);
    }

    @Test
    public void should_update_new_pokemon() {
        PokemonEntity created = savePokemon();
        created.setTotal(8);
        pokemonRepository.save(created);
        PokemonEntity updated = pokemonRepository.getPokemonByNameIgnoreCase("Bulbasaur");
        Assertions.assertEquals(8, updated.getTotal());
    }

    @Test
    public void should_get_pokemon_by_number() {
        savePokemon();
        List<PokemonEntity> list = pokemonRepository.getPokemonsByNumber(1);
        Assertions.assertEquals(1, list.size());
        Assertions.assertEquals("Bulbasaur", list.get(0).getName());
    }

    private PokemonEntity savePokemon(){
        PokemonEntity pokemonEntity = new PokemonEntity();
        pokemonEntity.setName("Bulbasaur");
        pokemonEntity.setAttack(49);
        pokemonEntity.setDefense(50);
        pokemonEntity.setGeneration(1);
        pokemonEntity.setHp(100);
        pokemonEntity.setLegendary(false);
        pokemonEntity.setSpAtk(8);
        pokemonEntity.setSpDef(10);
        pokemonEntity.setSpeed(50);
        pokemonEntity.setTotal(5);
        pokemonEntity.setNumber(1);
        pokemonEntity.setType1(TypeEnum.GRASS);
        return pokemonRepository.save(pokemonEntity);
    }
}
